package org.eclipse.ckeditor;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Miscellaneous utility methods.
 *  
 * @author Konstantin Zaitcev
 */
public class Utils {
	
	/**
	 * @param stream input stream
	 * @return string content of stream in UTF-8 encoding
	 */
	public static String getStringFromStream(InputStream stream) {
		return getStringFromStream(stream, "UTF-8");
	}

	/**
	 * @param stream input stream
	 * @return string content of stream in UTF-8 encoding
	 */
	public static String getStringFromStream(InputStream stream, String charset) {
		StringBuilder sb = new StringBuilder();
		try (InputStreamReader reader = new InputStreamReader(stream, charset)) {
			char[] cbuf = new char[1024];
			int len = 0;
			while ((len = reader.read(cbuf)) > 0) {
				sb.append(cbuf, 0, len);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return sb.toString();
	}
}
