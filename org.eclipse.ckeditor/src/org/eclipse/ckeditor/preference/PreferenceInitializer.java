package org.eclipse.ckeditor.preference;

import org.eclipse.ckeditor.Activator;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

public class PreferenceInitializer extends AbstractPreferenceInitializer {

	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore pref = Activator.getDefault().getPreferenceStore();
		
		pref.setDefault("lanuage", "en");
		for (String key: ToolbarPrefs.TOOLBAR.keySet()) {
			pref.setDefault("toolbar_" + key, true);
		}
		pref.setDefault("config_file_enable", false);
		pref.setDefault("config_file", "");
	}
}
