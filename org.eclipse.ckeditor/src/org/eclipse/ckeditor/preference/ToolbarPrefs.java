package org.eclipse.ckeditor.preference;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Toolbar preferences constants.
 * 
 * @author Konstantin Zaitcev
 */
public class ToolbarPrefs {
	public static Map<String, String> TOOLBAR = new LinkedHashMap<>();
	static {
		TOOLBAR.put("document", "'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates'");
		TOOLBAR.put("clipboard", "'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo'");
		TOOLBAR.put("editing", "'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt'");
		TOOLBAR.put("forms", "'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'");
		TOOLBAR.put("basicstyles", "'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat'");
		TOOLBAR.put("paragraph", "'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl'");
		TOOLBAR.put("links", "'Link','Unlink','Anchor'");
		TOOLBAR.put("insert", "'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe'");
		TOOLBAR.put("styles", "'Styles','Format','Font','FontSize'");
		TOOLBAR.put("colors", "'TextColor','BGColor'");
		TOOLBAR.put("tools", "'Maximize', 'ShowBlocks','-','About'");
	}
}
