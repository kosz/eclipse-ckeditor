package org.eclipse.ckeditor.preference;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.ckeditor.Activator;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * @author Konstantin Zaitcev
 */
public class WorkbenchPreferencePage extends PreferencePage implements IWorkbenchPreferencePage  {
	
	/** Lang preferences. */
	private Combo langs;
	private Map<String, Button> toolbars = new HashMap<>();
	private FileFieldEditor config;
	private Button configEnabled;

	@Override
	public void init(IWorkbench workbench) {
		IPreferenceStore pref = Activator.getDefault().getPreferenceStore();
		setPreferenceStore(pref);
		setDescription("Customize appearance of CKEditor");
	}

	@Override
	protected Control createContents(Composite parent) {
		Composite composite = createComposite(parent);

		createBasicGroup(composite);
		createToolbarGroup(composite);
		createAdvancedGroup(composite);
		
		return composite;
	}

	@Override
	public boolean performOk() {
		IPreferenceStore pref = getPreferenceStore();
		pref.setValue("language", LangPrefs.langs[langs.getSelectionIndex()][1]);
		for (Entry<String, Button> item: toolbars.entrySet()) {
			pref.setValue("toolbar_" + item.getKey(), item.getValue().getSelection());
		}
		pref.setValue("config_file_enable", configEnabled.getSelection());
		config.store();
		return true;
	}
	
	@Override
	protected void performDefaults() {
		IPreferenceStore pref = getPreferenceStore();
		langs.select(getLangIdxByCode(pref.getDefaultString("language")));
		for (Entry<String, Button> item: toolbars.entrySet()) {
			item.getValue().setSelection(pref.getDefaultBoolean("toolbar_" + item.getKey()));
		}
		config.loadDefault();
		configEnabled.setSelection(pref.getDefaultBoolean("config_file_enable"));
	}

	private void createToolbarGroup(Composite composite) {
		Group group = new Group(composite, SWT.LEFT);
		group.setText("Toolbar");
		group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		group.setFont(composite.getFont());
		GridLayout layout = new GridLayout();
		layout.numColumns = 6;
		group.setLayout(layout);
		
		IPreferenceStore pref = getPreferenceStore();
		for (Entry<String, String> toolbarItem: ToolbarPrefs.TOOLBAR.entrySet()) {
			Button button = new Button(group, SWT.CHECK);
			button.setSelection(pref.getBoolean("toolbar_" + toolbarItem.getKey()));
			toolbars.put(toolbarItem.getKey(), button);
			createLabel(group, toolbarItem.getKey(), toolbarItem.getValue());
		}
	}

	private void createAdvancedGroup(Composite composite) {
		final Group group = new Group(composite, SWT.LEFT);
		group.setText("Advanced");
		group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		group.setFont(composite.getFont());
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		group.setLayout(layout);
		
		final Composite composite3 = new Composite(group, SWT.NONE);
		composite3.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		GridLayout layout3 = new GridLayout();
		layout3.numColumns = 2;
		composite3.setLayout(layout3);
		configEnabled = new Button(composite3, SWT.CHECK);
		createLabel(composite3, "Enable custom configuration", "This option will load configuration from config.js file and ignore toolbar settings");

		final Composite composite2 = new Composite(group, SWT.NONE);
		composite2.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		config = new FileFieldEditor("config_file", "Configuration file", true, composite2);
		config.setPreferenceStore(getPreferenceStore());
		config.load();
		
		configEnabled.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				config.setEnabled(configEnabled.getSelection(), composite2);
			}
		});
		configEnabled.setSelection(getPreferenceStore().getBoolean("config_file_enable"));
		config.setEnabled(configEnabled.getSelection(), composite2);
	}
	
	private Label createLabel(Composite composite, String text, String tooltip) {
		Label label = new Label(composite, SWT.NONE);
		label.setText(text);
		if (tooltip != null) {
			label.setToolTipText(tooltip);
		}
		return label;
	}

	private void createBasicGroup(Composite composite) {
		Group group = new Group(composite, SWT.LEFT);
		group.setText("Basic");
		group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		group.setFont(composite.getFont());
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		group.setLayout(layout);

		IPreferenceStore pref = getPreferenceStore();

		Label langsLabel = new Label(group, SWT.NONE);
		langsLabel.setText("Language: ");
		langs = new Combo(group, SWT.READ_ONLY);

		for (int i = 0; i < LangPrefs.langs.length; i++) {
			langs.add(LangPrefs.langs[i][0], i);
		}
		langs.select(getLangIdxByCode(pref.getString("language"))); ;
	}

	private Composite createComposite(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridData data = new GridData(GridData.FILL_BOTH);
		composite.setLayoutData(data);
		composite.setFont(parent.getFont());
		GridLayout layout = new GridLayout();
		layout.marginWidth = 0;
		layout.marginHeight = 0;
		layout.verticalSpacing = 10;
		composite.setLayout(layout);
		return composite;
	}
	
	private int getLangIdxByCode(String code) {
		for (int i = 0; i < LangPrefs.langs.length; i++) {
			if (LangPrefs.langs[i][1].equals(code)) {
				return i;
			}
		}
		return 0;
	}
}
